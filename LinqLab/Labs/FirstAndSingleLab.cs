﻿using System.Collections.Generic;

namespace LinqLab.Labs
{
    public class FirstAndSingleLab : LabBase
    {
        public FirstAndSingleLab() : base() { }
        public FirstAndSingleLab(IEnumerable<Sample> samples) : base(samples) { }

        /// <summary>
        /// 搜尋UserName等於demo的資料使用First
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameFirst_demo() => FirstByUserName("demo");

        /// <summary>
        /// 搜尋UserName等於skilltree的資料使用First
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameFirst_skilltree() => FirstByUserName("skilltree");

        /// <summary>
        /// 搜尋UserName等於skilltree的資料使用FirstOrDefault
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameFirstOrDefault_skilltree() => FirstOrDefaultByUserName("skilltree");

        /// <summary>
        /// 搜尋UserName等於demo的資料使用Single
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameSingle_demo() => SingleByUserName("demo");

        /// <summary>
        /// 搜尋UserName等於bill的資料使用Single
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameSingle_bill() => SingleByUserName("bill");

        /// <summary>
        /// 搜尋UserName等於bill的資料使用SingleOrDefault
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameSingleOrDefault_bill() => SingleOrDefaultByUserName("bill");

        /// <summary>
        /// 搜尋UserName等於skilltree的資料使用SingleOrDefault
        /// </summary>
        /// <returns></returns>
        public Sample FindUserNameSingleOrDefault_skilltree() => SingleOrDefaultByUserName("skilltree");
    }
}
