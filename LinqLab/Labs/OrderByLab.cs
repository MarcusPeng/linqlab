﻿using System.Collections.Generic;
using System.Linq;

namespace LinqLab.Labs
{
    public class OrderByLab : LabBase
    {
        public OrderByLab() : base() { }
        public OrderByLab(IEnumerable<Sample> samples) : base(samples) { }


        public List<Sample> 請使用Id升冪排序() => Source.OrderBy(x => x.Id).ToList();

        public List<Sample> 請使用Id降冪排序() => Source.OrderByDescending(x => x.Id).ToList();

        public List<Sample> 請使用Price昇冪排序後再使用Id昇冪排序() => Source.OrderBy(x => x.Price).ThenBy(x => x.Id).ToList();
    }
}
