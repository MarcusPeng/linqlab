﻿using System.Collections.Generic;
using System.Linq;

namespace LinqLab.Labs
{
    public class WhereLab : LabBase
    {
        public WhereLab() : base() { }
        public WhereLab(IEnumerable<Sample> samples) : base(samples) { }

        public List<Sample> 搜尋Id大於8的資料() => WhereIdBigerThen(8).ToList();

        public List<Sample> 搜尋Price等於200的資料() => WherePriceEquals(200).ToList();

        public List<Sample> 搜尋UserName開頭為d的資料() => WhereUserNameStarstWith("d").ToList();

        public List<Sample> 搜尋UserName包含e的資料() => WhereUserNameContents("e").ToList();

        public List<Sample> 搜尋UserName結尾為o的資料() => WhereUserNameEndsWith("o").ToList();

        public List<Sample> 搜尋UserName是demo和joey的資料() => WhereUserNameIn(new[] { "demo", "joey" }).ToList();

        public bool 判斷是否有Id等於99的資料() => IsIdEquals(99);
    }
}
