﻿using System.Collections.Generic;
using System.Linq;

namespace LinqLab.Labs
{
    public abstract class LabBase
    {
        #region Property
        protected static IEnumerable<Sample> Source { get; set; } 
        #endregion

        #region Constructor
        public LabBase()
        {
            Source = SampleDate.Samples;
        }
        public LabBase(IEnumerable<Sample> samples)
        {
            Source = samples;
        }
        #endregion

        protected Sample FirstByUserName(string userName) => Source.First(x => x.UserName.Equals(userName));
        protected Sample FirstOrDefaultByUserName(string userName) => Source.FirstOrDefault(x => x.UserName.Equals(userName));
        protected Sample SingleByUserName(string userName) => Source.Single(x => x.UserName.Equals(userName));
        protected Sample SingleOrDefaultByUserName(string userName) => Source.SingleOrDefault(x => x.UserName.Equals(userName));

        protected IEnumerable<Sample> WhereIdBigerThen(int id) => Source.Where(x => x.Id > id);
        protected IEnumerable<Sample> WherePriceEquals(int price) => Source.Where(x => x.Price.Equals(price));
        protected IEnumerable<Sample> WhereUserNameStarstWith(string s) => Source.Where(x => x.UserName.StartsWith(s));
        protected IEnumerable<Sample> WhereUserNameContents(string s) => Source.Where(x => x.UserName.Contains(s));
        protected IEnumerable<Sample> WhereUserNameEndsWith(string s) => Source.Where(x => x.UserName.EndsWith(s));
        protected IEnumerable<Sample> WhereUserNameIn(IEnumerable<string> userNames) => Source.Where(x => userNames.Contains(x.UserName));
        protected bool IsIdEquals(int id) => Source.Any(x => x.Id.Equals(id));

        protected IEnumerable<IGrouping<string, Sample>> GroupByUserName() => Source.GroupBy(x => x.UserName);
        protected IEnumerable<IGrouping<TempObj, Sample>> GroupByTempObj() => Source.GroupBy(x => new TempObj { UserName = x.UserName, CreateTime = x.CreateTime });

        protected IEnumerable<Sample> Take(int count) => Source.Take(count);
        protected IEnumerable<Sample> Skip(int count) => Source.Skip(count);
        protected IEnumerable<Sample> SkipThenTake(int skipCount, int takeCount) => Source.Skip(skipCount).Take(takeCount);

    }
}
