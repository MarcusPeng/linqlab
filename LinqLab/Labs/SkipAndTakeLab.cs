﻿using System.Collections.Generic;
using System.Linq;

namespace LinqLab.Labs
{
    public class SkipAndTakeLab : LabBase
    {
        public SkipAndTakeLab() : base() { }
        public SkipAndTakeLab(IEnumerable<Sample> samples) : base(samples) { }

        public List<Sample> 跳過五筆資料() => Skip(5).ToList();

        public List<Sample> 取得五筆資料() => Take(5).ToList();

        public List<Sample> 跳過五筆取得兩筆() => SkipThenTake(5, 2).ToList();
    }
}
