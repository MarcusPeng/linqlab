﻿using System.Collections.Generic;
using System.Linq;

namespace LinqLab.Labs
{
    public class GroupByLab : LabBase
    {
        public GroupByLab() : base() { }
        public GroupByLab(IEnumerable<Sample> samples) : base(samples) { }

        /// <summary>
        /// 使用UserName作為群組依據
        /// </summary>
        /// <returns></returns>
        public new Dictionary<string, List<Sample>> GroupByUserName() => base.GroupByUserName().ToDictionary(x => x.Key, x => x.ToList());

        /// <summary>
        /// 使用UserName和CreateTime作為群組依據
        /// </summary>
        /// <returns></returns>
        public new Dictionary<TempObj, List<Sample>> GroupByTempObj() => base.GroupByTempObj().ToDictionary(x => x.Key, x => x.ToList());
    }
}
